import java.util.ArrayList;

public class Illness {
    private String name;
    private ArrayList<Medication> medicationList = new ArrayList<>();

    public Illness(String name) {
        this.name = name;
    }

    public void addMedication(Medication medication) {
        medicationList.add(medication);
    }

    public String getInfos() {
        return "Nom de la maladie : " + this.name + ", " + medicationList.get(0).getInfos();
    }

}
