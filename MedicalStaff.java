import java.util.UUID;

public abstract class MedicalStaff extends Person implements Care {
    private String employeeId;

    public MedicalStaff(String name, int age, String socialSecurityNumber){
        super(name, age, socialSecurityNumber);
        this.employeeId = UUID.randomUUID().toString();
    }

    abstract String getRole();

    public String getEmployeeId() {
        return this.employeeId;
    }
}
