
public class Hospital {
    public static void main(String[] args) {
        // médicaments
        Medication paracetamol = new Medication("Paracetamol", "2 par jour");
        Medication medicamentRhume = new Medication("Médicament pour le rhume", "3 par jour");

        // Test Medication
        System.out.println(paracetamol.getInfos());

        // Maladies
        Illness grippe = new Illness("Grippe");
        grippe.addMedication(paracetamol);
        Illness rhume = new Illness("Rhume");
        rhume.addMedication(medicamentRhume);

        // Test Illness
        System.out.println(rhume.getInfos());

        // Patients
        Patient patient1 = new Patient("Phillippe", 30, "987654321");
        patient1.addIllness(grippe);
        Patient patient2 = new Patient("Sandrine", 40, "123456789");
        patient2.addIllness(rhume);

        // Test Patient
        patient1.getInfo();

        // Docteur
        Doctor doctor1 = new Doctor("Sophie", 48, "999888777", "Docteur");
        Doctor doctor2 = new Doctor("Lucas", 50, "666777888", "Médecin");

        Nurse nurse1 = new Nurse("Julie", 32, "222333444");
        Nurse nurse2 = new Nurse("Patrick", 37, "555666777");

        // Test des docteurs et des nurses
        doctor1.careForPatient(patient1);
        nurse1.careForPatient(patient1);
        doctor2.careForPatient(patient2);
        nurse2.careForPatient(patient2);
    }
}
