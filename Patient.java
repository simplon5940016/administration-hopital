import java.util.ArrayList;
import java.util.UUID;

public class Patient extends Person {
    private String patientId;
    private ArrayList<Illness> illnessList = new ArrayList<>();

    public Patient(String name, int age, String socialSecurityNumber) {
        super(name, age, socialSecurityNumber);
        this.patientId = UUID.randomUUID().toString();
    }

    public void addIllness(Illness illness) {
        this.illnessList.add(illness);
    }

    public void getInfo() {
        System.out.println("Nom : " + super.getName() + ", Age : " + super.getAge() + ", Numéro de sécurité social : "
                + super.getSocialSecurityNumber() + ", ID : " + this.patientId + " ; " + illnessList.get(0).getInfos());
    }
}
