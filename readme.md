# Java - Administration d'un hôpital

## Fonctionnalités

- Enregistrement des patients et stockage de leurs informations médicales.
- Gestion des médecins et des infirmiers, incluant leurs informations personnelles et professionnelles.
- Recherche et consultation des informations des patients et du personnel médical.

## Compilation et Exécution

Pour compiler et exécuter le programme :

```bash
java -cp . Hospital.java
```
