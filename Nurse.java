public class Nurse extends MedicalStaff {

    public Nurse(String name, int age, String socialSecurityNumber) {
        super(name, age, socialSecurityNumber);
    }

    public String getRole() {
        return "Nurse ";
    }

    @Override
    public void careForPatient(Patient patient) {
        System.out.println(getRole() + " " + super.getName() + " cares for " + patient.getName());
    }

}
