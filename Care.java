public interface Care {
    public void careForPatient(Patient patient);

    public default void recordPatientVisit() {
        System.out.println("Par défault");
    }
}