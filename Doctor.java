public class Doctor extends MedicalStaff {
    private String speciality;

    public Doctor(String name, int age, String socialSecurityNumber, String speciality) {
        super(name, age, socialSecurityNumber);
        this.speciality = speciality;
    }

    public String getRole() {
        return this.speciality;
    }

    @Override
    public void careForPatient(Patient patient) {
        System.out.println(getRole() + " " + super.getName() + " cares for " + patient.getName());
    }
}
